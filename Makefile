hw4: hw4.o
	gcc -o hw4 -g hw4.o -lcrypto

hw4.o: hw4.c
	gcc -g -c -Wall hw4.c -I/usr/local/opt/openssl/include

clean:
	rm -f *.o hw4
