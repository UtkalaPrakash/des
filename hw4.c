#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <getopt.h>
#include <math.h>

FILE *fTable;
unsigned long long int keys[3][16];
int IP[8][8], E[8][6], P[8][4], PC1[8][7], PC2[8][7], V[16], S[32][16];
int IP_inv[8][8] = {{27, 31, 26, 30, 25, 29, 24, 28}, 
					{59, 63, 58, 62, 57, 61, 56, 60}, 
					{19, 23, 18, 22, 17, 21, 16, 20}, 
					{51, 55, 50, 54, 49, 53, 48, 52}, 
					{11, 15, 10, 14, 9, 13, 8, 12}, 
					{43, 47, 42, 46, 41, 45, 40, 44}, 
					{3, 7, 2, 6, 1, 5, 0, 4}, 
					{35, 39, 34, 38, 33, 37, 32, 36}};

void pusage(char *string) {
	// This prints the usage and ends the program on occurrence of error.
	fprintf(stderr, "Malformed %s\n", string);
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t./hw4 tablecheck -t=tablefile\n");
	fprintf(stderr, "\t./hw4 encrypt -k=key -t=tablefile [file]\n");
	fprintf(stderr, "\t./hw4 decrypt -k=key -t=tablefile [file]\n");
	fprintf(stderr, "\t./hw4 encrypt3 -k=key3 -t=tablefile [file]\n");
	fprintf(stderr, "\t./hw4 decrypt3 -k=key3 -t=tablefile [file]\n");
	exit(1);
}

FILE* fileCheck(char *file_name) {
	// This function checks whether input file is readable and its existence.
	if (strlen(file_name) == 0) pusage("command");
	if ( access(file_name, F_OK) == -1 ) {
	    fprintf(stderr, "Input file %s doesn't exist\n", file_name);
	    exit(1);
	}

	struct stat file_check;
	stat(file_name, &file_check);
	if ( S_ISDIR(file_check.st_mode) ) {
	    fprintf(stderr, "Input file %s is a directory\n", file_name);
	    exit(1);
	}

	if ( access(file_name, R_OK) == -1 ) {
	    fprintf(stderr, "Cannot read input file %s, permission denied\n", file_name);
	    exit(1);
	}

	FILE *file_handler = fopen(file_name, "r");
	if ( file_handler == NULL) {
            fprintf(stderr, "Input file %s cannot be open\n", file_name);
            exit(1);
        }
	return file_handler;
}

void checkIP() {
	int i=0, k[65] = {0};
	char check[4] = "";
	fread(check, 3, 1, fTable);
	if (strcmp(check, "IP=") != 0) pusage("tablefile");
	for (i=0; i<64; i++) {
		char input[3] = "";
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (IP) Less numbers read\n");
			exit(1);
		}
		if (input[1] == ',' || input[1] == '\n') {
			input[1] = '\0';
		} else {
			fseek(fTable, 1, SEEK_CUR);
		}
		int val = atoi(input);
		IP[i/8][i%8] = val;
		IP_inv[(val-1)/8][(val-1)%8] = i+1;
		k[IP[i/8][i%8]]++;
	}

	for (i=1; i<65; i++) {
		if (k[i] > 1) {
			fprintf(stderr, "Error: (IP) %d is repeating\n", i);
			exit(1);
		}
	}
}

void checkE() {
	int i=0, k[33] = {0};
	fseek(fTable, 2, SEEK_CUR);
	for (i=0; i<48; i++) {
		char input[3] = "";
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (E) Less numbers read\n");
			exit(1);
		}
		if (input[1] == ',' || input[1] == '\n') {
			input[1] = '\0';
		} else {
			fseek(fTable, 1, SEEK_CUR);
		}
		E[i/6][i%6] = atoi(input);
		k[E[i/6][i%6]]++;
	}

	for (i=1; i<33; i++) {
		if (k[i] > 2) {
			fprintf(stderr, "Error: (E) %d is repeated more than twice\n", i);
			exit(1);
		} else if (k[i] == 0) {
			fprintf(stderr, "Erroe: (E) %d is not used once\n", i);
			exit(1);
		}
	}
}

void checkP() {
	int i=0, k[33] = {0};
	fseek(fTable, 2, SEEK_CUR);
	for (i=0; i<32; i++) {
		char input[3] = "";
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (P) Less numbers read\n");
			exit(1);
		}
		if (input[1] == ',' || input[1] == '\n') {
			input[1] = '\0';
		} else {
			fseek(fTable, 1, SEEK_CUR);
		}
		P[i/4][i%4] = atoi(input);
		k[P[i/4][i%4]]++;
	}

	for (i=1; i<33; i++) {
		if (k[i] > 1) {
			fprintf(stderr, "%d is repeated twice in P\n", i);
			exit(1);
		}
	}
}

void checkS() {
	int i=0, k, j[16] = {0};
	for (k=0; k<8; k++) {
		//printf("\nS box %d\n", k);
		fseek(fTable, 3, SEEK_CUR);
		for (i=0; i<64; i++) {
			char input[3] = "";
			fread(input, 2, 1, fTable);
			if (input[0] >= 65 && input[0] <= 90) {
				fseek(fTable, -2, SEEK_CUR);
				fprintf(stderr, "Error: (S%d) Less numbers read\n", k+1);
				exit(1);
			}
			if (input[1] == ',' || input[1] == '\n') {
				input[1] = '\0';
			} else {
				fseek(fTable, 1, SEEK_CUR);
			}
			S[(k * 4) + i/16][i%16] = atoi(input);
			//printf("%d,", S[i/16][i%16]);
			j[atoi(input)]++;
			if ( i%16 == 15) {
				int l;
				for (l=0; l<16; l++) {
					if (j[l] == 0) {
						fprintf(stderr, "Error: (S%d) Number %d didnot occur 4 times\n", k+1, l);
						exit(1);
					}
					j[l] = 0;
				}
			}
		}
	}
}

void checkV() {
	int i = 0, ones = 0, twos = 0;
	fseek(fTable, 2, SEEK_CUR);
	for (i=0; i<16; i++) {
		char input[3] = "";
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (V) Less numbers read\n");
			exit(1);
		}
		input[1] = '\0';
		V[i] = atoi(input);
		if (V[i] == 1) {
			ones++;
			if (ones > 4) {
				fprintf(stderr, "Error: (V) Number of one's is greater than 4\n");
				exit(1);
			}
		} else if (V[i] == 2) {
			twos++;
			if (twos > 12) {
				fprintf(stderr, "Error: (V) Number of two's is greater than 12\n");
				exit(1);
			}
		} else {
			fprintf(stderr, "Error: (V) %d shouldnot occur. Please check the table\n", V[i]);
			exit(1);
		}
	}
}

void checkPC1() {
	int i=0, k[65] = {0};
	char input[3] = "";
	fseek(fTable, 4, SEEK_CUR);
	for (i=0; i<56; i++) {
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (PC1) Less numbers read\n");
			exit(1);
		}
		if (input[1] == ',' || input[1] == '\n') {
			input[1] = '\0';
		} else {
			fseek(fTable, 1, SEEK_CUR);
		}
		PC1[i/7][i%7] = atoi(input);
		k[PC1[i/7][i%7]]++;
	}

	for (i=1; i<65; i++) {
		if ( k[i] == 0 && i%8 != 0 ) {
			fprintf(stderr, "Error: (PC1) %d is not found in the table\n", i);
			exit(1);
		}
	}
}

void checkPC2() {
	int i=0, k[57] = {0};
	char input[3] = "";
	fseek(fTable, 4, SEEK_CUR);
	for (i=0; i<48; i++) {
		fread(input, 2, 1, fTable);
		if (input[0] >= 65 && input[0] <= 90) {
			fseek(fTable, -2, SEEK_CUR);
			fprintf(stderr, "Error: (PC2) Less numbers read\n");
			exit(1);
		}
		if (input[1] == ',' || input[1] == '\n') {
			input[1] = '\0';
		} else {
			fseek(fTable, 1, SEEK_CUR);
		}
		PC2[i/6][i%6] = atoi(input);
		k[PC2[i/6][i%6]]++;
	}

	for (i=1; i<57; i++) {
		if (k[i] > 1) {
			fprintf(stderr, "Error: (PC2) %d is repeated many times\n", i);
			exit(1);
		}
	}
}

unsigned long long int hex(char *string) {
	// Converts HEX string to Hex numbers
	int i;
	unsigned long long int val =0;
	for (i=0; i< 16; i++) {
		char hex = *string;
		if (!(hex >= 48 && hex <= 57) && !(hex >= 97 && hex <= 102)) pusage("key");
		int temp = strtol(&hex, NULL, 16);
		unsigned long long int temp1 = (temp * pow(2,(4*(15-i))));
		val += temp1;
		string++;
	}
	return val;
}

void print_num(unsigned long long int val, int size) {
	// Prints Input bits in ascii values
	int i=size-1;
	for (;i>=0;i--) {
		int p = (val >> (i*8)) & 0xff;
		printf("%c", p);
	}
}

unsigned long long int xor(unsigned long long int a, unsigned long long int b, int size) {
	// This function XOR 2 integers bitwise
	int i =0;
	unsigned long long int c = 0;
	for (i=0; i< size; i++) c += (((a >> i) & 0x1) ^ ((b >> i) & 0x01)) * pow(2, i);
	return c;
}

unsigned long long int permutation(char *pm, unsigned long long input) {
	// This is the block which permutes the input according to the permutation matrix specified
	int i,j;
	if (strcmp(pm, "PC1") == 0) {
		int k = 55;
		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<7; j++) {
				unsigned long long int mask = pow(2, (64 - PC1[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
	} else if (strcmp(pm, "PC2") == 0) {
		int k = 47;
		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<6; j++) {
				unsigned long long int mask = pow(2, (56 - PC2[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
 	} else if (strcmp(pm, "E") == 0) {
 		int k = 47;
 		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<6; j++) {
				unsigned long long int mask = pow(2, (32 - E[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
 	} else if (strcmp(pm, "P") == 0) {
 		int k = 31;
 		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<4; j++) {
				unsigned long long int mask = pow(2, (32 - P[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
 	} else if (strcmp(pm, "IP") == 0) {
 		int k = 63;
 		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<8; j++) {
				unsigned long long int mask = pow(2, (64 - IP[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
 	} else if (strcmp(pm, "IP_inv") == 0) {
 		int k = 63;
 		unsigned long long int val = 0;
		for (i=0; i<8; i++) {
			for (j=0; j<8; j++) {
				unsigned long long int mask = pow(2, (64 - IP_inv[i][j]));
				val += (((input & mask)/mask) << k);
				k--;
			}
		}
		return val;
 	}
 	return 0;
}

void keygen(unsigned long long int key, int num) {
	// This file generates key according to the key given
	int i;
	unsigned long long int val = permutation("PC1", key);
	unsigned long long int c = (val >> 28) & (0x0fffffff);
	unsigned long long int d = val & (0x0fffffff);
	fprintf(stderr, "(C0,D0)=%014llx\n", val);
	for (i=1; i<=16; i++) {
		int v = V[i-1], bit;
		long int mask; 
		if (v == 1) {
			mask = 0x8000000;
		} else {
			mask = 0xc000000;
		}
		bit = (c & mask) >> (28-v);
		c = ((c << v) | bit) & (0x0fffffff);
		bit = (d & mask) >> (28-v);
		d = ((d << v) | bit) & (0x0fffffff);
		val = (c << 28) | d;
		keys[num][i-1] = permutation("PC2", val);
		fprintf(stderr, "(C%d,D%d)=%07llx%07llx\n", i, i, c, d);
		fprintf(stderr, "k%d=%012llx\n", i, keys[num][i-1]);
	}
}

unsigned long long int function(unsigned long long int left,unsigned long long int right,int iteration,int num,int index, int print) {
	// Function block of DES is implemented here
	int i;
	unsigned long long int val = 0, output;
	output = permutation("E", right);
	output = xor(output, keys[num][iteration], 48);

	for (i=0; i<8; i++) {
		int byte = (output >> (6*(7-i))) & 0x3f;
		int row = (2 * ((byte & 0x20) >> 5)) + (byte & 0x1);
		int col = (byte >> 1) & 0xf;
		int temp = S[(i*4) + row][col];
		val += (temp * pow(2,(4*(7-i))));
	}
	output = permutation("P", val);
	output = xor(output, left, 32);
	if (print) fprintf(stderr, "(L%d,R%d)=%08llx%08llx\n", index, index, right, output);
	return output;
}

unsigned long long int encrypt_fun(unsigned long long int input, int num, int print) {
	// This function encrypts the input string by calling appropriate key
	int i=0;
	input = permutation("IP", input);
	unsigned long long int L = (input >> 32) & 0xffffffff;
	unsigned long long int R = input & 0xffffffff;
	if (print) fprintf(stderr, "(L0,R0)=%08llx%08llx\n", L, R);
	for (i=0; i<16; i++) {
		unsigned long long int temp = L;
		L = R;
		R = function(temp, R, i, num, i+1, print);
	}
	input = (R << 32) | L;
	input = permutation("IP_inv", input);
	return input;
}

unsigned long long int decrypt_fun(unsigned long long int input, int num, int print) {
	// This function decrypts the input string by calling appropriate key
	int i;
	input = permutation("IP", input);
	unsigned long long int L = (input >> 32) & 0xffffffff;
	unsigned long long int R = input & 0xffffffff;
	if (print) fprintf(stderr, "(L0,R0)=%08llx%08llx\n", L, R);
	for (i=15; i>=0; i--) {
		unsigned long long int temp = L;
		L = R;
		R = function(temp, R, i, num, 16-i, print);
	}
	input = (R << 32) | L;
	input = permutation("IP_inv", input);
	return input;
}

int main(int argc, char **argv) {
	if (argc < 3) pusage("command");
	unsigned long long int key[3];
	char tablefile[50] = "", option[15] = "";

	if (	strcmp(*(argv+1), "tablecheck") != 0 && 
			strcmp(*(argv+1), "encrypt") != 0 && 
			strcmp(*(argv+1), "decrypt") != 0 && 
			strcmp(*(argv+1), "encrypt3") != 0 && 
			strcmp(*(argv+1), "decrypt3") != 0 ) {
	    pusage("command");
    } else {
	    strcpy(option, *(argv+1));
	}

	// Handle input arguments
	int index = 0, opt;
	static struct option options[] = {
        	{"k",    required_argument, 0,  'k' },
        	{"t",    required_argument, 0,  't' },
			{NULL, 0, NULL, 0}
	};
	while ((opt = getopt_long_only(argc, argv,"tk::", options, &index)) != -1) {
		if ( opt == 't' ) {
		    sprintf(tablefile, "%s", optarg);
		} else if ( opt == 'k' ) {
			if (strcmp(*(argv+1), "encrypt") == 0 || strcmp(*(argv+1), "decrypt") == 0) {
				//key[0] = strtoll(optarg, NULL, 16);
				key[0] = hex(optarg);
			} else {
				if (strlen(optarg) < 48) pusage("key size");
				key[0] = hex(optarg);
				key[1] = hex(optarg+16);
				key[2] = hex(optarg+32);
				//printf("%llx %llx %llx\n", key[0], key[1], key[2]);
			}
		} else {
	 	    pusage("command");
		}
    }

    fTable = fileCheck(tablefile);
    checkIP();
    checkE();
    checkP();
    checkS();
    checkV();
    checkPC1();
    checkPC2();
    fclose(fTable);
    if (strcmp(option, "encrypt") == 0) {
    	FILE *file_handler;
    	if (argc == 5) {
    		file_handler = fileCheck(*(argv+4));
    	} else {
    		file_handler = stdin;
    	}
    	keygen(key[0], 0);
    	int print = 1;
    	while (!feof(file_handler)) {
    		int end = 0;
    		unsigned long long int inp = 0;
    		int i;
    		for (i=7; i>=0; i--) {
    			unsigned char input;
    			fread(&input, 1, 1, file_handler);
    			if (feof(file_handler)) {
    				end = 1;
    				break;
    			}
    			unsigned long long int ascii = ((int) input) * pow(2, (i*8));
    			inp += ascii;
    		}
    		if (inp) {
    			inp = encrypt_fun(inp, 0, print);
    			print = 0;
    			print_num(inp, 8);
    		}
    		if (end ==1) break;
    	}
    } else if (strcmp(option, "decrypt") == 0) {
    	FILE *file_handler;
    	if (argc == 5) {
    		file_handler = fileCheck(*(argv+4));
    	} else {
    		file_handler = stdin;
    	}
    	keygen(key[0], 0);
    	int print = 1;
    	while (!feof(file_handler)) {
    		int end = 0;
    		unsigned long long int inp = 0;
    		int i;
    		for (i=7; i>=0; i--) {
    			unsigned char input;
    			fread(&input, 1, 1, file_handler);
    			if (feof(file_handler)) {
    				end = 1;
    				break;
    			}
    			unsigned long long int ascii = ((int) input) * pow(2, (i*8));
    			inp += ascii;
    		}
    		
    		if (inp) {
    			inp = decrypt_fun(inp, 0, print);
    			print = 0;
    			print_num(inp, 8);
    		}
    		if (end ==1) break;
    	}
    } else if (strcmp(option, "encrypt3") == 0) {
    	keygen(key[0], 0);
    	keygen(key[1], 1);
    	keygen(key[2], 2);
    	FILE *file_handler;
    	if (argc == 5) {
    		file_handler = fileCheck(*(argv+4));
    	} else {
    		file_handler = stdin;
    	}
    	int print = 1;
    	while (!feof(file_handler)) {
    		unsigned long long int inp = 0;
    		int i, end =0;
    		for (i=7; i>=0; i--) {
    			unsigned char input;
    			fread(&input, 1, 1, file_handler);
    			if (feof(file_handler)) {
    				end = 1;
    				break;
    			}
    			unsigned long long int ascii = ((int) input) * pow(2, (i*8));
    			inp += ascii;
    		}
    		
    		if (inp) {
    			inp = encrypt_fun(inp, 0, print);
    			inp = decrypt_fun(inp, 1, print);
    			inp = encrypt_fun(inp, 2, print);
    			print = 0;
    			print_num(inp, 8);
    		}
    		if (end ==1) break;
    	}
    } else if (strcmp(option, "decrypt3") == 0){
    	keygen(key[0], 0);
    	keygen(key[1], 1);
    	keygen(key[2], 2);
    	FILE *file_handler;
    	if (argc == 5) {
    		file_handler = fileCheck(*(argv+4));
    	} else {
    		file_handler = stdin;
    	}
    	int print =1;
    	while (!feof(file_handler)) {
    		unsigned long long int inp = 0;
    		int i, end =0;
    		for (i=7; i>=0; i--) {
    			unsigned char input;
    			fread(&input, 1, 1, file_handler);
    			if (feof(file_handler)) {
    				end = 1;
    				break;
    			}
    			unsigned long long int ascii = ((int) input) * pow(2, (i*8));
    			inp += ascii;
    		}
    		if (inp) {
    			inp = decrypt_fun(inp, 2, print);
    			print = 0;
    			inp = encrypt_fun(inp, 1, print);
    			inp = decrypt_fun(inp, 0, print);
    			print_num(inp, 8);
    		}
    		if (end == 1) break;
    	}
    }
	return 0;
}
